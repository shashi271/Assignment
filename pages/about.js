import Head from "next/head";
import { useRouter } from "next/router";
import { FormattedMessage, useIntl } from "react-intl";
import Contact from "../component/contact/contact";
import styles from "../styles/Home.module.css";
import Link from "next/link";
export default function About (){
  const {locales} = useRouter()
  const router = useRouter()
  const intl = useIntl();

  const title = intl.formatMessage({ id: "about" });
    return(
      <>
      <header>
      <div className={styles.languages} style={{color:'red', padding:"10px",display:'flex'}}>
        {[...locales].sort().map((locale) => (
          <Link key={locale} href={router.asPath} locale={locale}>
           <div  style={{padding:"10px",cursor:'pointer' }}>{locale=="en"? "English": locale=="hi"? "Hindi" : locale=="ta"? "Tamil" : "French"}</div> 
          </Link>
         
        ))}
      </div>
    </header>
       <main className={styles.main} style={{height:'100%'}} >
            
        {/* <FormattedMessage id="about" values={{ b: (chunks) => <b>{chunks}</b> }} /> */}
       <center>
        {title}
        <Contact/>
        
       </center>
       <div style={{color:'green'}}>
       <Link href="/details">details</Link>
       </div>
       
        </main>   </>)
      
}