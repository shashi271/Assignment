import '../styles/globals.css'
import { useRouter } from "next/router";
import { IntlProvider } from "react-intl";
import en from "../lang/en.json";
import hi from "../lang/hi.json";
import fr from "../lang/fr.json";
import ta from "../lang/ta.json";
function MyApp({ Component, pageProps }) {
  const { locale } = useRouter();
  const messages = {
    en,
    hi,
    fr,
    ta,
  };
  return  <IntlProvider locale={locale} messages={messages[locale]}> <Component {...pageProps} /></IntlProvider>
 
}

export default MyApp
