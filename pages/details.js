import { FormattedMessage, useIntl } from "react-intl";
import styles from "../styles/Home.module.css";
export default function Detail(){
    const intl = useIntl();

  const title = intl.formatMessage({ id: "details" });
    return (<div className={styles.main}>
        {title}
    </div>)
}

