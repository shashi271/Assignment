import Head from "next/head";
import Link from "next/link";
import Router, { useRouter } from "next/router";
import { FormattedMessage, useIntl } from "react-intl";

import styles from "../styles/Home.module.css";

export default function Home({ dir }) {
  const { locales } = useRouter();

  const intl = useIntl();

  const title = intl.formatMessage({ id: "greeting" });
 

  return (
    <div className={styles.container}>
      <Head>
        <title>{title}</title>
        <meta name="description"  />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <header>
        <div className={styles.languages} style={{color:'red', padding:"10px",display:'flex'}}>
          {[...locales].sort().map((locale) => (
            <Link key={locale} href="/" locale={locale}>
             <div  style={{padding:"10px",cursor:'pointer' }}>{locale=="en"? "English": locale=="hi"? "Hindi" : locale=="ta"? "Tamil" : "French"}</div> 
            </Link>
           
          ))}
        </div>
      </header>

      <main dir={dir} className={styles.main}>
        <h1 className={styles.title}>
          {title}
        </h1>
        <h2  style={{color:"blue"}}>             
            <Link  href="/about" >
              About
            </Link>
        </h2>
      </main>
    </div>
  );
}
