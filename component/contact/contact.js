import { FormattedMessage, useIntl } from "react-intl";
export default function Contact(){
    const intl = useIntl();

  const title = intl.formatMessage({ id: "contact" });
    return(<div>
       {title}
    </div>)
}